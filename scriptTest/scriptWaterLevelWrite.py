import paho.mqtt.client as mqtt
import time
import random

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("lucas.chalscl@gmail.com/LeWell")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = mqtt.Client()
client.username_pw_set(username="lucas.chalscl@gmail.com",password="caac")
client.on_connect = on_connect
client.on_message = on_message

client.connect("maqiatto.com", 1883, 60)
state = False
while 1:
    # state = not state    
    level = float(random.uniform(10.5, 75.5))
    print("write "+  str(level))
    client.publish("lucas.chalscl@gmail.com/LeWell", level)
    time.sleep(5)