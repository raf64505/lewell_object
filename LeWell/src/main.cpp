#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include <HCSR04.h>



// Parameters Pump
const int digPin = 2;

// Parameters Sensor
const byte triggerPin = 5;
const byte echoPin = 18;

#define SOUND_SPEED 340
#define TRIG_PULSE_DURATION_US 10

long ultrason_duration;
float heightWater = 0;
float heightWaterMin = 5;
float heightWaterRepump = 10;
float distance_cm;
String statePump = "True";


// Connect Wifi
const char* ssid = "Redmi Note 9 Pro";
const char* password = "caaccaca";

// Connect Broker
const char* mqtt_broker = "maqiatto.com";
const char* topicPump = "lucas.chalscl@gmail.com/LeWellPump";
const char* topicData = "lucas.chalscl@gmail.com/LeWell";
const char* mqtt_username = "lucas.chalscl@gmail.com";
const char* mqtt_password = "caac";
const int mqtt_port = 1883;

WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void onOffPumpe(){
  int stateActualPump = digitalRead(digPin);

  Serial.println("stateActualPump");
  Serial.println(stateActualPump);
  Serial.println("statePump");
  Serial.println(statePump);
  Serial.println("heightWater");
  Serial.println(heightWater);

  if(statePump && heightWater){
    Serial.println("not null");
    if(stateActualPump == 1) {
      Serial.println("state high");
      if(statePump == "True" && heightWater > heightWaterMin){
        Serial.println("true and good");
          digitalWrite(digPin, HIGH);
      }
      else {
        Serial.println("nope");
          digitalWrite(digPin, LOW);
      }
    } else {
      Serial.println("state low");
      if(statePump == "True" && heightWater > heightWaterRepump){
        Serial.println("true and good repump");
          digitalWrite(digPin, HIGH);
      }
      else {
        Serial.println("nope repump");
          digitalWrite(digPin, LOW);
      }
    }

  } else {
    Serial.println("null");
    digitalWrite(digPin, LOW);
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.println("------- New Message Received -------");
  Serial.print("Topic: ");
  Serial.println(topic);

  String message = "";

  // Convertir le payload en une chaîne de caractères
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    message += (char)payload[i];
  }

  Serial.println();

  // Traitement pour le topic Pump
  if (strcmp(topic, topicPump) == 0) {
    statePump = message;
  }

  onOffPumpe();
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect("ESP32Client", mqtt_username, mqtt_password)) {
      Serial.println("connected");
      client.subscribe(topicPump);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);
  HCSR04.begin(triggerPin, echoPin);

  pinMode(digPin, OUTPUT);

  setup_wifi();
  client.setServer(mqtt_broker, mqtt_port);
  client.setCallback(callback);
}

double measureDistance() {
   double* distance = HCSR04.measureDistanceCm();
   return distance[0];

}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  double distance = measureDistance();
  heightWater = distance;
  onOffPumpe();
  Serial.println(distance);

  // Publish distance to MQTT broker
  char distance_str[10];
  dtostrf(distance, 5, 2, distance_str);
  client.publish(topicData, distance_str);

  // Your other logic here
  delay(500);
}
